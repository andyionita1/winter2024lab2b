//Import random & scanner
import java.util.Random;
import java.util.Scanner;

public class Wordle
{
	//Method to choose a random word from a list of arrays containing 20+ words, of 5 letters & no repetitive character
	public static String generateWord(){
		
		//Create an array of 20+ words that have 5 letter each and no repetitive characters
		String[] wordsList = {"BLOCK", "JUDGE", "TABLE", "STRAW", "AUDIO", "READY", "CHIEF", "TOUCH", "ROAST", "EARLY", "STEAM", "STEAK", "COUNT", "FRAME", "ADMIN", "ABUSE", "BLOND", "BUYER", "BUILD", "BRING", "TOWEL", "DWARF", "GLIDE"};
		
		Random random = new Random();
		
		//Choose a random word among the wordsList array and return it		
		int randomFromList = random.nextInt(wordsList.length);	
		return wordsList[randomFromList];
	}
	
	//Method to verify if the character is inside the word
	public static boolean letterInWord(String word, char letter){
		
		//Check if the char letter is contained inside the String word
		for (int i = 0; i < word.length(); i++)
		{
			if (word.charAt(i) == letter)
			{
				//Return true if the char letter is contained inside the String word
				return true;
			}
		}
		
		//Return false if the char letter is NOT contained inside the String word
		return false;
	}
	
	//Method to verify if the character is in the right spot of the word
	public static boolean letterInSlot(String word, char letter, int position){
		
		//Check if the char letter is contained inside the right spot of the String word
		for (int i = 0; i < word.length(); i++)
		{
			if (word.charAt(i) == letter && i == position)
			{
				//Return true if the char letter is contained inside the right spot of the String word
				return true;
			}
		}
		
		//Return false if the char letter is NOT contained inside the right spot of the String word
		return false;
	}
	
	//Method to assign a color (green, yellow or white) depending on the position of the char
	public static String[] guessWord(String answer, String guess){
		
		String[] colorsArray = new String[5];
		
		//Loop that can find out the right colors assigned to the characters
		for (int i = 0; i < answer.length(); i++)
		{
			//If it's at the correct spot, then the character will turn green
			if(letterInSlot(answer, guess.charAt(i), i)) {
				colorsArray[i] = "green";
			} //If it's at the wrong spot, then the character will turn yellow 
			else if (letterInWord(answer, guess.charAt(i)))
			{
				colorsArray[i] = "yellow";
			} else { //If the character is not there, then the character will turn white
				colorsArray[i] = "white";
			}
		}
		
		//Return the array made that shows the order of the characters' colors (Green, yellow, white)
		return colorsArray;
	}
	
	//Method that visually applies the colors to the characters
	public static void presentResults(String word, String[] colours){
		
		//Loop that actually APPLIES the colors to the characters in the cmd
		for (int i = 0; i < colours.length; i++) {
			//Will apply green
			if (colours[i].equals("green")) {
				System.out.print("\u001B[32m" + word.charAt(i));
			}	//Will apply yellow 
			else if (colours[i].equals("yellow")) {
				System.out.print("\u001B[33m" + word.charAt(i));
			}	//Will apply white
			else if (colours[i].equals("white")) {
				System.out.print("\u001B[0m" + word.charAt(i));
			}
		}
	}
	
	//Method that makes the user input his guessed word
	public static String readGuess(){
		
		//Create a scanner called "reader"
		Scanner reader = new Scanner(System.in);
		
		//Ask the user to guess & input a word
		System.out.println("Guess a word: ");
		String guessedWord = reader.nextLine();
		
		//Make sure the word the user is inputing is 5 characters long, otherwise make the user retry
		while (guessedWord.length() != 5)
		{
			System.out.println("Please enter a 5 letter word.");
			guessedWord = reader.nextLine();
		}
		
		//Turn the word the user guessed into uppercase
		String upperCaseGuessedWord = guessedWord.toUpperCase();
		
		//Return the uppercase word the user guessed
		return upperCaseGuessedWord;

	}
	
	//Method that overall runs the game
	public static void runGame(String word){
		
		//Create variables describing the amount of attempts the user has made
		int attempts = 1;
		
		//Turn a String guessedWord into the returned value of the method readGuess()
		String guessedWord = readGuess();
		
		//Make a loop that applies while the guessed word isn't equal to the target word and while the user hasn't reached the maximum attempts
		while (!(guessedWord.equals(word)) && attempts < 6)
		{
			//Make the user retry & guess
			String[] colorsArray = guessWord(word, guessedWord);
			presentResults(guessedWord, colorsArray);
			guessedWord = readGuess();
			
			//Increase the amount of user attempts by 1 everytime the loop runs
			attempts++;
		}
		
		//If the user has found the word, it will print a congratulations message
		if (guessedWord.equals(word))
		{
			System.out.println("You found the word!");
		} else 
		{	//If the user has NOT found the word, it will print a message explaining he failed
			System.out.println("You failed to find the word in under 6 attempts. The word was " + word);
		}
		
	}
	
	public static void main (String[] args){		
		
		//Run the game
		String target = generateWord();		
		runGame(target);
	}
	
	
}