//Import a Java scanner
import java.util.Scanner;

public class Hangman
{	
	
	//Method that checks if the character is in the word. If it is, return the position of the char in word.
	public static int isLetterInWord(String word, char c)
	{
		//Create a variable for the position of the character, in the String word (default set to 0)
		int positionChar = 0;
		
		//Create a loop that runs until the position of the character in the String word, is less than the length of the String word
		while(positionChar < word.length())
		{	
			//Check if the character entered, at the set position, corresponds with the character that's in the String word
			if(word.charAt(positionChar) == c)
			{
				//Return the position of the character in the String word
				return positionChar;
			}
			
			//Add 1 to the positionChar variable, making the character advance by 1 in the word
			positionChar++;
		}
		
		//When the loop finishes, return -1
		return -1;
	}
	
	public static void printWord(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {		
		
		//Create a boolean array letterArray[] to store boolean values of each letter
		boolean[] letterArray = {letter0, letter1, letter2, letter3};
		
		//Loop that checks if each variable in letterArray[] is true. If true print corresponding character, if not print "_"
		for (int i = 0; i < letterArray.length; i++) {
			if (letterArray[i]) {
				System.out.print(word.charAt(i));
			} else {
				System.out.print("_");
			}
		}
	}

	
	public static void runGame(String word)
    {
        //Create a scanner
		Scanner reader = new Scanner(System.in);
		
		//Create a variable for the amount of times
        int amountOfTimesTried = 0;

        //Create variables to see if every letter corresponds with the correct character. Set them all to false by default
		boolean letter0 = false;
        boolean letter1 = false;
        boolean letter2 = false;
        boolean letter3 = false;

        //Make a loop that runs while all the letters are set to false, and while the amount of times that have been tried is below 6
		while (!(letter0 && letter1 && letter2 && letter3) && amountOfTimesTried < 6)
        {
		   System.out.println("Enter a letter: ");
            
			//Store what the user wrote in a String letter
			String letter = reader.next();
			
			//Convert the String letter into a character c
            char c = letter.charAt(0);

           //Create a variable that stores the return values of the function isLetterInWord
		   int wordWithLetters = isLetterInWord(word, c);

            //Use if statements to verify is the characters fit accordingly to the letters			
			if(wordWithLetters == 0){
                letter0 = true;
            }
            else if(wordWithLetters == 1)
            {
                letter1 = true;
            } else if(wordWithLetters == 2)
            {
                letter2 = true;
            } else if (wordWithLetters == 3)
            {
                letter3 = true;
            } else {
                System.out.println("It doesn't have the letter " + c);
                amountOfTimesTried++;
            }

            //Tell the user what the result of the word is
			System.out.print("Your result is: ");
            printWord(word, letter0, letter1, letter2, letter3);
        }

        //If all the letters are true, tell the user he won the game and found the word.
		if (letter0 && letter1 && letter2 && letter3){
            System.out.println("You won, you found the word!");
        } else {
            //If all the letters are NOT true, tell the user he has exceeded the 6 tries, and has lost the game.
			System.out.println("You lost, you've exceeded 6 tries!");
        }
    }
}