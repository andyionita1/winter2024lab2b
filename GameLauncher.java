//Import a Java Scanner
import java.util.Scanner;

public class GameLauncher {
	
	public static void main(String[] args) {
		
		//Create a Scanner
		Scanner reader = new Scanner(System.in);
		
		//Ask the user which game to play
		System.out.println("Press 1 to play Hangman. Press 2 to play Wordle");
		
		//Store what user chose in an int selectedGame variable
		int selectedGame = reader.nextInt();
		
		//Run game depending on what the user chose
		if (selectedGame == 1) {
			runHangmanGame();		
		} else if (selectedGame == 2) {
			runWordleGame();
		} else if (selectedGame > 2 || selectedGame < 1) {
			System.out.println("This game does not exist.");
		}
	}
	
	//Run Hangman game
	public static void runHangmanGame() {
		
		//Create a Scanner
		Scanner reader = new Scanner(System.in);
		
		//Tell the user to enter a word with 4 characters.
		System.out.println("Enter a word, with 4 characters:");
		
		//Store what the use wrote in a String word variable
		String word = reader.nextLine();		
		
		//Run the game if the word is 4 characters long
		if (word.length() == 4)
		{
			//Call out the runGame() function and store the String word variable in (What the user has typed)
			Hangman.runGame(word);
		} else
		{
			//If the word isn't 4 characters long, tell the user the word should be 4 characters long, and not run the game
			System.out.println("Your word should be 4 characters long.");
		}	
	}
	
	//Run Wordle game
	public static void runWordleGame() {
		String target = Wordle.generateWord();		
		Wordle.runGame(target);
	}
}